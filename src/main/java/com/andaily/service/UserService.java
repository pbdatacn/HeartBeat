package com.andaily.service;

import com.andaily.domain.dto.user.UserDto;
import com.andaily.domain.dto.user.UserFormDto;
import com.andaily.domain.dto.user.UserListDto;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author Shengzhao Li
 */
public interface UserService extends UserDetailsService {

    UserDto loadUserDto(String guid);

    UserListDto loadUserListDto(UserListDto listDto);

    UserFormDto loadUserFormDto(String guid);
}