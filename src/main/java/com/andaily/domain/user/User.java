package com.andaily.domain.user;

import com.andaily.domain.AbstractDomain;
import com.andaily.domain.shared.BeanProvider;
import com.andaily.infrastructure.PasswordHandler;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

/**
 * @author Shengzhao Li
 */
@Entity
@Table(name = "user_")
public class User extends AbstractDomain {


    private static final long serialVersionUID = -7873396364481790308L;

    private transient UserRepository userRepository = BeanProvider.getBean(UserRepository.class);

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    //Default user is initial when create database, do not delete
    @Column(name = "default_user")
    private boolean defaultUser = false;

    @Column(name = "last_login_time")
    private Date lastLoginTime;

    public User() {
    }

    public User(String username, String password, String phone, String email) {
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.email = email;
    }


    public List<Privilege> privileges() {
        return userRepository.findUserPrivileges(this);
    }

    public boolean defaultUser() {
        return defaultUser;
    }

    public String username() {
        return username;
    }

    public String password() {
        return password;
    }

    public String phone() {
        return phone;
    }

    public String email() {
        return email;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("{username='").append(username).append('\'');
        sb.append(", phone='").append(phone).append('\'');
        sb.append(", id='").append(id).append('\'');
        sb.append(", guid='").append(guid).append('\'');
        sb.append(", defaultUser='").append(defaultUser).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public User email(String email) {
        this.email = email;
        return this;
    }

    public User phone(String phone) {
        this.phone = phone;
        return this;
    }


    public User username(String username) {
        this.username = username;
        return this;
    }

    public String resetPassword() {
        String newOriginalPass = PasswordHandler.randomPassword();
        this.password = PasswordHandler.encryptPassword(newOriginalPass);
//        userRepository.saveOrUpdate(this);
        return newOriginalPass;
    }


    public Date lastLoginTime() {
        return lastLoginTime;
    }

    public void lastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
}