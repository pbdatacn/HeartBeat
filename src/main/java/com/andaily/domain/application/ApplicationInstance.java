package com.andaily.domain.application;

import com.andaily.domain.AbstractDomain;

import javax.persistence.*;

/**
 * A server instance that is need monitoring
 * 定义一个需要心跳监测的应用实例
 *
 * @author Shengzhao Li
 */
@Entity
@Table(name = "application_instance")
public class ApplicationInstance extends AbstractDomain {

    private static final long serialVersionUID = 1826152029135090793L;
    //实例名称
    @Column(name = "instance_name")
    private String instanceName;

    //Monitoring url: http://axxx.com/
    //监测地址URL
    @Column(name = "monitor_url")
    private String monitorUrl;

    /**
     * {@link #monitorUrl}  send request method,
     * default: GET
     */
    @Column(name = "request_method")
    @Enumerated(value = EnumType.STRING)
    private MonitorUrlRequestMethod requestMethod = MonitorUrlRequestMethod.GET;

    /**
     * 连接时超时的时间
     * 0,表示无超时
     */
    @Column(name = "max_connection_seconds")
    private int maxConnectionSeconds;

    //enabled or disabled
    //是否启用
    @Column(name = "enabled")
    private boolean enabled;

    /**
     * 心跳检测频率, 默认30秒
     */
    @Column(name = "frequency")
    @Enumerated(value = EnumType.STRING)
    private HeartBeatFrequency frequency = HeartBeatFrequency.THIRTY;

    /**
     * 若出现测试正常或不正常时提醒的邮件地址
     * 若有多个请用英文分号(;)分隔
     */
    @Column(name = "email")
    private String email;


    /**
     * Schedule中的任务名称,
     * 当启用该监听任务时, 将会有唯一对应的jobName
     */
    @Column(name = "job_name")
    private String jobName;

    /**
     * 备注信息
     */
    @Column(name = "remark")
    private String remark;

    /**
     * Default
     */
    public ApplicationInstance() {
    }


    public MonitorUrlRequestMethod requestMethod() {
        return requestMethod;
    }

    public ApplicationInstance requestMethod(MonitorUrlRequestMethod requestMethod) {
        this.requestMethod = requestMethod;
        return this;
    }

    public String instanceName() {
        return instanceName;
    }

    public ApplicationInstance instanceName(String instanceName) {
        this.instanceName = instanceName;
        return this;
    }

    public String monitorUrl() {
        return monitorUrl;
    }

    public ApplicationInstance monitorUrl(String monitorUrl) {
        this.monitorUrl = monitorUrl;
        return this;
    }

    public int maxConnectionSeconds() {
        return maxConnectionSeconds;
    }

    public ApplicationInstance maxConnectionSeconds(int maxConnectionSeconds) {
        this.maxConnectionSeconds = maxConnectionSeconds;
        return this;
    }

    public boolean enabled() {
        return enabled;
    }

    public ApplicationInstance enabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public HeartBeatFrequency frequency() {
        return frequency;
    }

    public ApplicationInstance frequency(HeartBeatFrequency frequency) {
        this.frequency = frequency;
        return this;
    }

    public String email() {
        return email;
    }

    public String[] emailAsArray() {
        return email.indexOf(";") > 0 ? email.split(";") : new String[]{email};
    }

    public ApplicationInstance email(String email) {
        this.email = email;
        return this;
    }

    public String jobName() {
        return jobName;
    }

    public ApplicationInstance jobName(String jobName) {
        this.jobName = jobName;
        return this;
    }

    public String remark() {
        return remark;
    }

    public ApplicationInstance remark(String remark) {
        this.remark = remark;
        return this;
    }
}