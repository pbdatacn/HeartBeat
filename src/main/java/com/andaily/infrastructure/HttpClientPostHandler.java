package com.andaily.infrastructure;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;

/**
 * 15-3-27
 *
 * @author Shengzhao Li
 */
public class HttpClientPostHandler extends HttpClientHandler {

    public HttpClientPostHandler(String url) {
        super(url);
    }

    @Override
    protected HttpRequestBase retrieveHttpRequest() {
        return new HttpPost(url);
    }
}
