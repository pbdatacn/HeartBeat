package com.andaily.infrastructure;

import com.andaily.domain.log.FrequencyMonitorLog;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper HttpClient operations
 * Default request method: GET
 *
 * @author Shengzhao Li
 */
public class HttpClientHandler {

    protected static final Logger LOGGER = LoggerFactory.getLogger(HttpClientHandler.class);
    //Convert mill seconds to second unit
    protected static final int MS_TO_S_UNIT = 1000;
    //Normal http response code
    protected static final int NORMAL_RESPONSE_CODE = 200;

    protected String url;

    protected int maxConnectionSeconds = 0;

    public HttpClientHandler(String url) {
        this.url = url;
    }

    public HttpClientHandler maxConnectionSeconds(int maxConnectionSeconds) {
        this.maxConnectionSeconds = maxConnectionSeconds;
        return this;
    }

    public FrequencyMonitorLog handleAndGenerateFrequencyMonitorLog() {
        FrequencyMonitorLog monitorLog = new FrequencyMonitorLog();

        final long start = System.currentTimeMillis();
        try {
            final HttpResponse response = sendRequest();

            monitorLog.normal(isNormal(response))
                    .responseSize(responseSize(response))
                    .costTime(costTime(start));

        } catch (Exception e) {
            monitorLog.costTime(costTime(start)).normal(false)
                    .remark(e.getClass().getSimpleName() + ": " + e.getMessage());
            LOGGER.debug("Send request to url[" + url + "] failed", e);
        }

        return monitorLog;
    }

    protected long responseSize(HttpResponse response) {
        return response.getEntity().getContentLength();
    }

    protected long costTime(long start) {
        return System.currentTimeMillis() - start;
    }

    protected boolean isNormal(HttpResponse response) {
        return response.getStatusLine().getStatusCode() == NORMAL_RESPONSE_CODE;
    }

    protected HttpResponse sendRequest() throws Exception {
        HttpRequestBase request = retrieveHttpRequest();
        addRequestParams(request);
        HttpClient client = retrieveHttpClient();
        return client.execute(request);
    }

    protected DefaultHttpClient retrieveHttpClient() {
        return new DefaultHttpClient();
    }

    protected HttpRequestBase retrieveHttpRequest() {
        return new HttpGet(url);
    }

    protected void addRequestParams(HttpRequestBase request) {
        if (this.maxConnectionSeconds <= 0) {
            return;
        }
        final BasicHttpParams params = new BasicHttpParams();
        params.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, this.maxConnectionSeconds * MS_TO_S_UNIT);

		//设置数据传输时间
		//避免虽然连接成功但服务器端长时间无返回值，导致达不到预期监控效果的情况
		params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, this.maxConnectionSeconds * MS_TO_S_UNIT);
        request.setParams(params);
    }

}